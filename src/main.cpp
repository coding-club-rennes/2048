/*
** main.cpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 14:13:06 2015 Alexis Bertholom
// Last update Tue Feb 23 00:02:13 2016 baille_l
*/

#include <unistd.h>
#include "SDLDisplay.hpp"
#include "Input.hpp"
#include "Colors.hpp"
#include "RNG.hpp"
bool RNG::_initialized = false;

int		main()
{
  SDLDisplay	display("2048", 800, 800);
  Input		input;
  int		tab[4][4];

  display.clearScreen();
  display.putRect(300, 200, 100, 50, Colors::Violet);
  display.putGrid(tab, 4, 4);
  display.refreshScreen();
  while (!(input.shouldExit()) && !(input.getKeyState(SDL_SCANCODE_ESCAPE)))
    {
      input.flushEvents();
    }
  return (0);
}
