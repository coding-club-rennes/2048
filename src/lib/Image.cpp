/*
** Image.cpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 12:44:19 2015 Alexis Bertholom
// Last update Mon Feb 22 23:51:03 2016 baille_l
*/

#include <cstring>
#include <SDL2/SDL.h>
#include "SDLError.hpp"
#include "Image.hpp"
#include "Colors.hpp"

Image::Image(Uint w, Uint h) : _allocd(true)
{
  if (!(this->_img = SDL_CreateRGBSurface(0, w, h, 32, 0, 0, 0, 0)))
    throw (SDLError("SDL surface creation failed"));
  this->_size = w * h;
}

Image::Image(SDL_Surface* img, Image::Type type)
{
  if (!img)
    throw (Error("Image: nullptr"));
  this->_size = img->w * img->h;
  if (type == Image::Wrap)
    {
      this->_allocd = false;
      this->_img = img;
      return ;
    }
  this->_allocd = true;
  if (!(this->_img = SDL_CreateRGBSurface(0, img->w, img->h, sizeof(Color) << 3, 0, 0, 0, 0)))
    throw (SDLError("SDL surface creation failed"));
  SDL_BlitSurface(img, NULL, this->_img, NULL);
}

Image::~Image()
{
  if (this->_allocd)
    SDL_FreeSurface(this->_img);
}

void		Image::putPixel(Uint x, Uint y, Color color)
{
  if ((int)x < this->_img->w && (int)y < this->_img->h)
    *((Color*)this->_img->pixels + sizeof(Color) * (y * this->_img->w + x)) = color;
}

void		Image::putRect(Uint x, Uint y, Uint w, Uint h, Color color)
{
  SDL_Rect	r;

  r.x = x;
  r.y = y;
  r.w = w;
  r.h = h;
  SDL_FillRect(this->_img, &r, color);
}

void		Image::putGrid(int tab[4][4], int x, int y)
{
  SDL_Rect	r;
  int		width = this->_img->w;
  int		height = this->_img->h;
  int		i = 0;
  int		j = 0;
  int		color;
  
  this->fill(0xa49381);
  r.w = width / x - 10;
  r.h = width / y - 10;
  while (j < y)
    {
      i = 0;
      r.y = height / y * j + 5;
      while (i < x)
	{
	  r.x = width / x * i + 5;
	  switch(tab[j][i])
	    {
	    case 0:
	      color = 0xFFFFFF;
	      break;
	    case 2:
	      color = 0xede3d9;
	    break;
	    case 4:
	      color = 0xece0c8;
	      break;
	    case 8:
	      color = 0xf2b179;
	      break;
	    case 16:
	      color = 0xf59563;
	      break;
	    case 32:
	      color = 0xf57c5f;
	      break;
	    case 64:
	      color = 0xf35f39;
	      break;
	    case 128:
	      color = 0xedcf72;
		break;
	    case 256:
	      color = 0xedcf72;
		break;
	    case 512:
	      color = 0xedc850;
		break;
	    case 1024:
	      color = 0xedc53f;
		break;
	    default :
	      color = 0;
	      break;
	    }
	  SDL_FillRect(this->_img, &r, color);
	  i++;
	}
      j++;
    }
}

void		Image::blit(Uint x, Uint y, Image& img, Uint w, Uint h)
{
  SDL_Rect	r;

  r.x = x;
  r.y = y;
  r.w = w;
  r.h = h;
  SDL_BlitSurface(img.getSurface(), &r, this->_img, NULL);
}

void		Image::clear()
{
  memset(this->_img->pixels, 0, this->_size * sizeof(Color));
}

void		Image::fill(Color color)
{
  Color*	ptr = (Color*)this->_img->pixels;

  for (Uint i = 0; i < this->_size; ++i)
    *(ptr++) = color;
}

SDL_Surface*	Image::getSurface()
{
  return (this->_img);
}
